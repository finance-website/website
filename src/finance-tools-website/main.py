from sys import argv
from asyncio import run
from fastapi import (
    FastAPI,
    Request
)
from fastapi.responses import (
    HTMLResponse
)
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from hypercorn.config import Config
from hypercorn.asyncio import serve
from hypercorn.middleware import HTTPToHTTPSRedirectMiddleware

#input args
style_version = str(argv[1])
pi_graph_version = str(argv[2])
#   OPT. --dev flag to indicate a dev run
is_dev = False
for a in argv:
    if str(a) == "--dev":
        is_dev = True
style_path = f"css/color_theme-{style_version}.min.css"
pi_graph_path = f"js/PIGraphApp-v{pi_graph_version}.bundle.js"

website = FastAPI()
website.mount("/app/static", StaticFiles(directory="static"), name="static")
web_config = Config.from_toml(f"/app/config/config.toml")
templates = Jinja2Templates(directory="templates")
page_urls = {
    "home_url": "/",
    "about_url": "/about",
    "tools_url": "/tools",
    "pi_graph_url": "/tools/pi-graph"
}

@website.get("/", response_class=HTMLResponse)
async def home(request: Request):
    ctx = {
        "request": request,
        "style_path": style_path,
        **page_urls
    }
    return templates.TemplateResponse("tools.html", ctx)

@website.get("/about", response_class=HTMLResponse)
async def home(request: Request):
    ctx = {
        "request": request,
        "style_path": style_path,
        **page_urls
    }
    return templates.TemplateResponse("about.html", ctx)

@website.get("/tools", response_class=HTMLResponse)
async def home(request: Request):
    ctx = {
        "request": request,
        "style_path": style_path,
        **page_urls
    }
    return templates.TemplateResponse("tools.html", ctx)

@website.get("/tools/pi-graph", response_class=HTMLResponse)
async def home(request: Request):
    ctx = {
        "request": request,
        "style_path": style_path,
        "pi_graph_path": pi_graph_path,
        "req_pi_graph": True,
        **page_urls
    }
    return templates.TemplateResponse("pi-graph.html", ctx)

if is_dev:
    run(serve(website, web_config))
else:
    redir_api = HTTPToHTTPSRedirectMiddleware(website, host="jonrizk.com")
    run(serve(redir_api, web_config))
