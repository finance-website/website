from setuptools import (
    setup,
    find_packages
)

install_req = [
    "hypercorn==0.11.2",
    "fastapi==0.63.0",
    "aiofiles==0.6.0",
    "jinja2==2.11.3"
]

setup(
    name="website",
    python_requires="==3.8.*",
    packages=find_packages("src"),
    install_requires=install_req
)