FROM python:3.8-slim as compile
ARG THEME_VERS
ARG PI_GRAPH_APP_VERS

ENV APP="/app"
ENV STYLE_VERS=$THEME_VERS
ENV PI_GRAPH_VERS=$PI_GRAPH_APP_VERS

# install tools & make dirs
RUN mkdir -p ${APP}/static/css && \
    mkdir -p ${APP}/static/js && \
    mkdir -p ${APP}/static/svg && \
    mkdir -p ${APP}/config && \
    mkdir -p ${APP}/web_certs

# copy build artifacts
COPY ./color_theme-${STYLE_VERS}.min.css ${APP}/static/css/color_theme-${STYLE_VERS}.min.css
COPY ./PIGraphApp-v${PI_GRAPH_VERS}.bundle.js ${APP}/static/js/PIGraphApp-v${PI_GRAPH_VERS}.bundle.js

# grab everything from source
COPY ./setup.py /
COPY ./src/finance-tools-website/ ${APP}
COPY ./src/finance-tools-website/templates/ ${APP}/templates
COPY ./src/svg/*.svg ${APP}/static/svg/

# setup env
RUN python3 /setup.py install
WORKDIR ${APP}

# make shell script to do var expansion
RUN echo "python3 main.py ${STYLE_VERS} ${PI_GRAPH_VERS} \$@" > run.sh

# start the service
ENTRYPOINT ["/bin/bash", "./run.sh"]
